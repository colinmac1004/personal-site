import React from 'react';
import '../../App.css';
import './Experience.css'

export default function Experience() {
    return (
        <>
            <h1 className='home'>Experience</h1>
            <br></br>
            <div className="info">
            <h2 align = "left">BTI360 Internship Summer 2022</h2>
            <p align = "left" >During this internship I worked with 4 other interns to design and deploy a full stack web application.
            We worked with technologies like Java Spring, Angular, AWS, Gitlab, and Jira. We learned things relating to all of the
            technologies mentioned before as well as the Agile software process and REST architecture.  It was an incredible learning
            experience as I was able to really get the feel of what it is like to work with a team of other software engineers while
            also learning best practices from the more experienced engineers who were kind enough to help us.</p>
        </div>
        </>
    )
}
