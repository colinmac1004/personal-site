export const MenuOptions =[
    {
        title:'Home',
        url:'/',
        cName: 'nav-links'
    },
    {
        title:'Projects',
        url:'/services',
        cName: 'nav-links'
    },
    {
        title:'Experience',
        url:'/experience',
        cName: 'nav-links'
    },
    {
        title:'Contact Me',
        url:'/contact',
        cName: 'nav-links'
    },
    {
        title:'Sign up',
        url:'#',
        cName: 'nav-links-mobile'
    },




]

